C=c++
FLAGS=-std=c++11

all:
	$(C) $(FLAGS) tool.cpp -o tool

install:
	install tool /usr/bin/tool