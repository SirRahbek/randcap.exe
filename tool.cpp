#include <iostream>


std::string rand_cap (const std::string& input) {
	std::string output;
	for (auto c : input)
		output += rand()%2 ? tolower(c) : toupper(c);
	return output;
}


int main(int argc, char const *argv[]) {
	srand( time(NULL) );
	std::string content;

	if (argc > 1) {
		for(int n=1 ; n<argc ; ++n)
			content += argv[n];
	}
	else {
		std::cin >> content;
	}

	std::cout << rand_cap(content);
	return 0;
}